using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSphere : MonoBehaviour
{
    [SerializeField] MeshRenderer myMesh;
    [SerializeField] TextMesh myTextmesh;
    /*public*/ int myOrder;

    public void MakeSphere(Material material, int order)
    {
        myMesh.material = material;
        myOrder = order;
        myTextmesh.text = myOrder.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Bullet"))
        {
            if(myOrder == PuzzleManager.Instance.reqOrder)
            {
                if(PuzzleManager.Instance != null)
                PuzzleManager.Instance.UpdateReqOrder();
                Destroy(this.gameObject);
            }
        }
    }
}
