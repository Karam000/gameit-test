using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereMaker : MonoBehaviour
{
    [SerializeField] PuzzleSphere spherePrefab;

    public List<PuzzleSphere> MakeSpheres(List<Material> materials,puzzleAreaBounds puzzleBounds, List<int> randomOrders)
    {
        List<PuzzleSphere> spheres = new List<PuzzleSphere>();

        for (int i = 0; i < 3; i++)
        {
            float randomX = Random.Range(puzzleBounds.minX, puzzleBounds.maxX);
            float randomY = Random.Range(puzzleBounds.minY, puzzleBounds.maxY);
            float randomZ = Random.Range(puzzleBounds.minZ, puzzleBounds.maxZ);

            Vector3 randomSpherePos = new Vector3(randomX, randomY, randomZ);
            PuzzleSphere SpawnedpuzzleSphere = Instantiate(spherePrefab, randomSpherePos, Quaternion.identity, this.transform);

            SpawnedpuzzleSphere.MakeSphere(materials[i], randomOrders[i]);

            spheres.Add(SpawnedpuzzleSphere);
        }

        return spheres;
    }
}
