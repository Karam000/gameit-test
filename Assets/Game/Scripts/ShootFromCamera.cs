using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFromCamera : MonoBehaviour
{
    [SerializeField] Rigidbody bulletPrefab;
    [SerializeField] Transform muzzle;
    [SerializeField] float bulletSpeed = 50;
    public void Shoot()
    {
        Rigidbody spawnedBullet = Instantiate(bulletPrefab, muzzle.position, bulletPrefab.transform.rotation);
        spawnedBullet.AddForce(spawnedBullet.transform.up * bulletSpeed,ForceMode.Impulse);
    }
}
