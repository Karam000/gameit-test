using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    [SerializeField] Bullet BulletPrefab;
    [SerializeField] Transform muzzle;
    [SerializeField] float bulletSpeed;
    [SerializeField] Image crosshair;
    [SerializeField] Sprite originalCrosshair;
    [SerializeField] Sprite lockedCrosshair;

    RaycastHit hit;
    GameObject prevHit;
    private void LateUpdate()
    {
        muzzle.transform.rotation = Camera.main.transform.rotation;

        if(Physics.Raycast(muzzle.position,muzzle.forward,out hit,Mathf.Infinity))
        {
            if (hit.collider.CompareTag("puzzleSphere"))
            {
                if (hit.collider.gameObject != prevHit)
                {
                    prevHit = hit.collider.gameObject;
                    crosshair.sprite = lockedCrosshair;
                } 
            }
        }
        else
        {
            prevHit = null;
            crosshair.sprite = originalCrosshair;
        }
    }
    public void Shoot()
    {
        Invoke("waitForAnimationGunPositioning", 0.15f);
    }

    private void waitForAnimationGunPositioning()
    {
        Bullet spawnedBullet = Instantiate(BulletPrefab, muzzle.position, Quaternion.identity);
        if(prevHit != null)
        {
            spawnedBullet.myDir = (prevHit.transform.position-muzzle.position).normalized;  
        }
        else
        {
            spawnedBullet.myDir = muzzle.forward;
        }
        spawnedBullet.mySpeed = bulletSpeed;
        spawnedBullet.ShootBullet();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(muzzle.position, muzzle.position + 10 * muzzle.transform.forward);
    }
}
