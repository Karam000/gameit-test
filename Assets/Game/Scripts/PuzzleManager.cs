using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    [SerializeField] UIManager UIManager;
    [SerializeField] SphereMaker sphereMaker;
    [SerializeField] List<Material> sphereMaterials;
    [SerializeField] BoxCollider puzzleCollider;

    [HideInInspector] public int reqOrder = 1;

    List<PuzzleSphere> SpawnedPuzzleSpheres = new List<PuzzleSphere>();
    int levelNumOfSpheres;

    public static PuzzleManager Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        else
            Destroy(this.gameObject);
    }

    [ContextMenu("Test start puzzle")]
    public void startPuzzle()
    {
        if (SpawnedPuzzleSpheres.Count == 0)
        {
            //if (SpawnedPuzzleSpheres.Count > 0)
            //{
            //    foreach (var sphere in SpawnedPuzzleSpheres)
            //    {
            //        Destroy(sphere);
            //    }
            //}
            puzzleAreaBounds puzzleBounds = getPuzzleArea();
            List<int> randomOrders = getRandomOrders();
            SpawnedPuzzleSpheres = sphereMaker.MakeSpheres(sphereMaterials, puzzleBounds, randomOrders);
            levelNumOfSpheres = SpawnedPuzzleSpheres.Count; 
        }
    }

    public void UpdateReqOrder()
    {
        reqOrder++;
        reqOrder = reqOrder%(levelNumOfSpheres+1);
        print(reqOrder);
    }
    private puzzleAreaBounds getPuzzleArea()
    {
        puzzleAreaBounds puzzleBounds = new puzzleAreaBounds();

        puzzleBounds.minX = puzzleCollider.bounds.min.x;
        puzzleBounds.minY = puzzleCollider.bounds.min.y;
        puzzleBounds.minZ = puzzleCollider.bounds.min.z;
        puzzleBounds.maxX = puzzleCollider.bounds.max.x;
        puzzleBounds.maxY = puzzleCollider.bounds.max.y;
        puzzleBounds.maxZ = puzzleCollider.bounds.max.z;

        return puzzleBounds;
    }

    private List<int> getRandomOrders()
    {
        List<int> orders = new List<int>();
        int rnd;

        for (int i = 0; i < 3; i++)
        {
            do
            {
                rnd = Random.Range(1, 4);
            } while (orders.Contains(rnd));

            orders.Add(rnd);
        }

        return ShuffleList(orders);
    }

    private List<int> ShuffleList(List<int> orders)
    {
        List<int> shuffledList = new List<int>();

        int rndIndex = Random.Range(0, orders.Count);

        int otherRndIndex;
        do
        {
            otherRndIndex = Random.Range(0, orders.Count);
        } while (otherRndIndex == rndIndex);


        int temp;

        temp = orders[rndIndex];
        orders[rndIndex] = orders[otherRndIndex];
        orders[otherRndIndex] = temp;

        shuffledList = orders;

        return shuffledList;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            startPuzzle();
            //UIManager.StartPuzzle();
        }
    }
}
public struct puzzleAreaBounds
{
    public float minX;
    public float minY;
    public float minZ;
    public float maxX;
    public float maxY;
    public float maxZ;
}
