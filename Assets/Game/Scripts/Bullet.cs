using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] Rigidbody myRb;
    [SerializeField] float lifeTime;
    [HideInInspector] public Vector3 myDir;
    [HideInInspector] public float mySpeed;
    private void Start()
    {
        Destroy(this.gameObject, lifeTime);
    }
    public void ShootBullet()
    {
        this.transform.forward = myDir;
        myRb.AddForce(mySpeed * this.transform.forward, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("puzzleSphere"))
        {
            Destroy(this.gameObject);
        }
    }
}
